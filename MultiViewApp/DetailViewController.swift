//
//  DetailViewController.swift
//  MultiViewApp
//
//  Created by Pranoti Kulkarni on 2/3/18.
//  Copyright © 2018 Pranoti Kulkarni. All rights reserved.
//

import UIKit
import SafariServices

class DetailViewController: UIViewController {

    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var videoTitleLabel: UILabel!
    @IBOutlet weak var videoDescriptionLabel: UILabel!
    
    var video: Video?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
        // Do any additional setup after loading the view.
    }
    
    func setUI() {
        videoImageView.image = video?.image
        videoTitleLabel.text = video?.title
        videoDescriptionLabel.text = video?.description
    }
   
    @IBAction func watchButtonTapped(_ sender: UIButton) {
        if let video = video {
            let videoURL = URL(string: video.url)
            let safariVC = SFSafariViewController(url: videoURL!)
            present(safariVC, animated: true, completion: nil)
        }
    }
    
}
