//
//  VideoCell.swift
//  MultiViewApp
//
//  Created by Pranoti Kulkarni on 2/3/18.
//  Copyright © 2018 Pranoti Kulkarni. All rights reserved.
//

import UIKit

protocol VideoCellDelegate {
    func didTapWatchLater(title: String)
    func didTapWatchNow(url: String)
}


class VideoCell: UITableViewCell {
    
    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var videoLabel: UILabel!
    
    var videoItem: Video!
    var delegate: VideoCellDelegate?
    
    @IBAction func watchLaterTapped(_ sender: UIButton) {
        delegate?.didTapWatchLater(title: videoItem.title)
    }
    
    @IBAction func watchNowTapped(_ sender: UIButton) {
        delegate?.didTapWatchNow(url: videoItem.url)
    }

    
    func setVideo(video: Video) {
        videoItem = video
        videoImageView.image = video.image
        videoLabel.text = video.title
    }
        
    
    
}
