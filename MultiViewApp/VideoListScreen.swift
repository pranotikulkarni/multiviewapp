//
//  VideoListScreen.swift
//  MultiViewApp
//
//  Created by Pranoti Kulkarni on 2/3/18.
//  Copyright © 2018 Pranoti Kulkarni. All rights reserved.
//

import UIKit
import SafariServices

class VideoListScreen: UIViewController {

    var videos: [Video] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        videos = createArray()
        
        //tableView.delegate = self
        //tableView.dataSource = self (or drag and drop in storyboard into yellow dot for delegate and datasource connection)

        // Do any additional setup after loading the view.
    }
    @IBOutlet weak var tableView: UITableView!
    
    func createArray() -> [Video] {
        var tempVideos: [Video] = []
        
        let video1 = Video(image: #imageLiteral(resourceName: "DJdIN-qVwAERjYA"), title: "First App", url: "https://www.youtube.com/watch?v=gN3FbNJ6_TY&t=309s", description: "IOS tutorials")
        let video2 = Video(image: #imageLiteral(resourceName: "hqdefault"), title: "IOS 2", url: "https://www.youtube.com/watch?v=gN3FbNJ6_TY&t=309s", description: "IOS tutorials")
        let video3 = Video(image: #imageLiteral(resourceName: "hqdefault-2"), title: "IOS 3", url: "https://www.youtube.com/watch?v=gN3FbNJ6_TY&t=309s", description: "IOS tutorials")
        let video4 = Video(image: #imageLiteral(resourceName: "DJdIN-qVwAERjYA"), title: "First App", url: "https://www.youtube.com/watch?v=gN3FbNJ6_TY&t=309s", description: "IOS tutorials")
        let video5 = Video(image: #imageLiteral(resourceName: "hqdefault"), title: "IOS 2", url: "https://www.youtube.com/watch?v=gN3FbNJ6_TY&t=309s", description: "IOS tutorials")
        let video6 = Video(image: #imageLiteral(resourceName: "hqdefault-2"), title: "IOS 3", url: "https://www.youtube.com/watch?v=gN3FbNJ6_TY&t=309s", description: "IOS tutorials")
        
        tempVideos.append(video1)
        tempVideos.append(video2)
        tempVideos.append(video3)
        tempVideos.append(video4)
        tempVideos.append(video5)
        tempVideos.append(video6)
        
        return tempVideos;
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "MasterToDetail" {
            let destVC = segue.destination as! DetailViewController
            destVC.video = sender as? Video
        }
    }

}

extension VideoListScreen: VideoCellDelegate{
    func didTapWatchNow(url: String) {
        let videoURL = URL(string: url)!
        let safariVC = SFSafariViewController(url : videoURL)
        present(safariVC, animated: true, completion: nil)
    }
    
    func didTapWatchLater(title: String) {
        let alertTitle = "Watch Now"
        let message = "\(title) added to Watch later list"
        let alert = UIAlertController.init(title: alertTitle, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}

extension VideoListScreen: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let video = videos[indexPath.row] //subsripting
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoCell") as! VideoCell
        
        cell.setVideo(video: video)
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let video = videos[indexPath.row]
        performSegue(withIdentifier: "MasterToDetail", sender: video)
    }
    
}

