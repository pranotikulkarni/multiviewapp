//
//  Video.swift
//  MultiViewApp
//
//  Created by Pranoti Kulkarni on 2/3/18.
//  Copyright © 2018 Pranoti Kulkarni. All rights reserved.
//

import Foundation
import UIKit

class Video{
    
    var image: UIImage
    var title: String
    var url : String
    var description: String
    
    init(image: UIImage, title: String, url: String, description: String ){
        self.image = image
        self.title = title
        self.url = url
        self.description = description
    }
}
